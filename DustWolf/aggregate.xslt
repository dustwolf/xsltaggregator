<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:feed="http://ggg.ctrl-alt-del.si/xmlns/feeds"
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:rss="http://backend.userland.com/rss2"
		version="1.0">

<!--
;;      Copyright 2009 Jure Sah
;;
;;      This file is part of XSLTaggregator.
;;
;;      XSLTaggregator is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.

This file contains the following third party contributions:
* a date sorting solution from: http://stackoverflow.com/questions/554909/problem-sorting-rss-feed-by-date-using-xsl
* a solution for a combined for-each loop by: David Carlisle

Kudos guys, thanks for your help!

-->

<xsl:output method="xml" indent="yes" />

<xsl:variable name="vrtfMonths">
 <m name="Jan" num="01"/>
 <m name="Feb" num="02"/>
 <m name="Mar" num="03"/>
 <m name="Apr" num="04"/>
 <m name="May" num="05"/>
 <m name="Jun" num="06"/>
 <m name="Jul" num="07"/>
 <m name="Aug" num="08"/>
 <m name="Sep" num="09"/>
 <m name="Oct" num="10"/>
 <m name="Nov" num="11"/>
 <m name="Dec" num="12"/>
</xsl:variable>

<xsl:template match="feed:feeds">

<xsl:variable name="vMonths" select="document('')/*/xsl:variable[@name='vrtfMonths']/*" />

<rss version="2.0">
 <channel>
  <title><xsl:value-of select="rss:title" /></title>
  <link><xsl:value-of select="rss:link" /></link>
  <atom:link href="{atom:link}" rel="self" />
  <atom:link href="{rss:link}" />
  <description><xsl:value-of select="rss:description" /></description>

  <xsl:if test="rss:language"><language><xsl:value-of select="rss:language" /></language></xsl:if>
  <xsl:if test="rss:copyright"><copyright><xsl:value-of select="rss:copyright" /></copyright></xsl:if>
  <xsl:if test="rss:managingEditor"><managingEditor><xsl:value-of select="rss:managingEditor" /></managingEditor></xsl:if>
  <xsl:if test="rss:webMaster"><webMaster><xsl:value-of select="rss:webMaster" /></webMaster></xsl:if>
  <xsl:if test="rss:pubDate">
   <pubDate><xsl:value-of select="rss:pubDate" /></pubDate>
   <atom:updated><xsl:value-of select="rss:pubDate" /></atom:updated>
  </xsl:if>
  <xsl:if test="rss:lastBuildDate"><lastBuildDate><xsl:value-of select="rss:lastBuildDate" /></lastBuildDate></xsl:if>
  <xsl:if test="rss:category"><category><xsl:value-of select="rss:category" /></category></xsl:if>
  <xsl:if test="rss:generator"><generator><xsl:value-of select="rss:generator" /> with DustWolf's XSLT-based aggregator</generator></xsl:if>
  <xsl:if test="rss:docs"><docs><xsl:value-of select="rss:docs" /></docs></xsl:if>
  <xsl:if test="rss:cloud"><cloud><xsl:value-of select="rss:cloud" /></cloud></xsl:if>
  <xsl:if test="rss:ttl"><ttl><xsl:value-of select="rss:ttl" /></ttl></xsl:if>
  <xsl:if test="rss:image"><image><xsl:value-of select="rss:image" /></image></xsl:if>
  <xsl:if test="rss:rating"><rating><xsl:value-of select="rss:rating" /></rating></xsl:if>
  <xsl:if test="rss:textInput"><textInput><xsl:value-of select="rss:textInput" /></textInput></xsl:if>
  <xsl:if test="rss:skipHours"><skipHours><xsl:value-of select="rss:skipHours" /></skipHours></xsl:if>
  <xsl:if test="rss:skipDays"><skipDays><xsl:value-of select="rss:skipDays" /></skipDays></xsl:if>

  <xsl:for-each select="document(feed:rss2)/rss/channel/item">

        <xsl:sort data-type="number" order="descending" select=
        "concat(substring(substring(pubDate,string-length(pubDate)-25),8,4),
                $vMonths[@name 
                        = 
                         substring(substring(current()/pubDate,string-length(current()/pubDate)-25),4,3)]/@num,

                substring(substring(pubDate,string-length(pubDate)-25),1,2),
                translate(substring(substring(pubDate,string-length(pubDate)-25),13,8),
                          ':',
                          ''
                          )
                )
         "/>

  <item>
   <xsl:if test="title"><title><xsl:value-of select="../title" />: <xsl:value-of select="title" /></title></xsl:if>
   <xsl:if test="link"><link><xsl:value-of select="link" /></link></xsl:if>
   <xsl:if test="description"><description><xsl:value-of select="description" /></description></xsl:if>
   <xsl:if test="author">
    <xsl:choose>
     <xsl:when test="author='*DustWolf'"><author>dustwolfy@gmail.com (DustWolf)</author></xsl:when>
     <xsl:otherwise><author><xsl:value-of select="author" /></author></xsl:otherwise>
    </xsl:choose>
   </xsl:if>
   <xsl:if test="category"><category><xsl:value-of select="category" /></category></xsl:if>
   <xsl:if test="comments"><comments><xsl:value-of select="comments" /></comments></xsl:if>
   <xsl:if test="enclosure"><enclosure><xsl:value-of select="enclosure" /></enclosure></xsl:if>
   <xsl:if test="guid">
    <xsl:choose>
     <xsl:when test="guid/@isPermaLink"><guid isPermaLink="{guid/@isPermaLink}"><xsl:value-of select="guid" /></guid></xsl:when>
     <xsl:otherwise><guid><xsl:value-of select="guid" /></guid></xsl:otherwise>
    </xsl:choose>
   </xsl:if>
   <xsl:if test="pubDate"><pubDate><xsl:value-of select="pubDate" /></pubDate></xsl:if>
   <xsl:if test="source"><source><xsl:value-of select="source" /></source></xsl:if>
  </item>
  </xsl:for-each>
 </channel>
</rss>
</xsl:template>

</xsl:stylesheet>
