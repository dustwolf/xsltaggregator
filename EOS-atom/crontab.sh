#!/bin/bash
if [ ! -f /tmp/EOS-atom.fresh ];
then
xsltproc `dirname $0`/atom.xslt `dirname $0`/atom.xml > /tmp/EOS-atom.tmp
touch /tmp/EOS-atom.fresh
fi
