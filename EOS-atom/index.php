<?php
/* ---------------------------------------------------------------------------
;;
;;      Copyright 2009 Jure Sah
;;
;;      This file is part of XSLTaggregator.
;;
;;      XSLTaggregator is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; -------------------------------------------------------------------------*/

$node = basename(getcwd());
$file = sys_get_temp_dir()."/".$node.".tmp";
if(file_exists($file)) {
 header("Content-type: application/atom+xml;charset=utf-8");
 $last_modified_time = filemtime($file);
 $etag = md5_file($file);
 
 header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
 header("Etag: $etag");
 
 passthru("cat ".$file);
 flush();
} else {
 header("HTTP/1.0 503 Temporarily Unavailable");
 header('Retry-After: 600');
}
unlink(sys_get_temp_dir()."/".$node.".fresh");
?>
