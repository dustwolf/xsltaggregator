<?php
/* ---------------------------------------------------------------------------
;;
;;      Copyright 2009 Jure Sah
;;
;;      This file is part of XSLTaggregator.
;;
;;      XSLTaggregator is free software: you can redistribute it and/or modify
;;      it under the terms of the GNU General Public License as published by
;;      the Free Software Foundation, either version 3 of the License, or
;;      (at your option) any later version.
;;
;;      Please refer to the README file for additional information.
;;
;; -------------------------------------------------------------------------*/

header("Content-type: application/rss+xml;charset=utf-8");
passthru("xsltproc aggregate.xslt aggregate.xml");
?>
