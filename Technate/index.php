<?php
header("Content-type: application/rss+xml;charset=utf-8");
$chan = new DOMDocument(); $chan->load('http://feeds.ctrl-alt-del.si/Technate-atom'); /* load channel */
$sheet = new DOMDocument(); $sheet->load('atom2rss.xsl'); /* use stylesheet from this page */
$processor = new XSLTProcessor();
$processor->registerPHPFunctions();
$processor->importStylesheet($sheet);
echo $processor->transformToXML($chan); /* transform to XML string (there are other options - see PHP manual)  */
?>
