<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:f="http://ggg.ctrl-alt-del.si/xmlns/feeds"
		xmlns:at="http://www.w3.org/2005/Atom"
		xmlns="http://www.w3.org/2005/Atom"
		version="1.0">

<xsl:output method="xml" indent="yes" />

<xsl:template match="f:feeds">
 <feed xmlns="http://www.w3.org/2005/Atom">
  <title><xsl:value-of select="at:title" /></title>
  <icon><xsl:value-of select="at:icon" /></icon>
  <link href="{at:link[@rel='self']/@href}" rel="self" />
  <link href="{at:link[not(@rel)]/@href}" />
  <xsl:for-each select="document(f:atom)/at:feed/at:entry">  
   <xsl:sort select="at:updated" data-type="text" order="descending" />
   <xsl:if test="position() = 1">
    <id><xsl:value-of select="at:id" /></id>
    <updated><xsl:value-of select="at:updated" /></updated>
   </xsl:if>
   <xsl:copy><xsl:copy-of select="@*|node()"/><xsl:apply-templates /></xsl:copy>
  </xsl:for-each>  
 </feed>
</xsl:template>

<xsl:template match='text()'/>

</xsl:stylesheet>
