#!/bin/bash
if [ ! -f /tmp/VillageUniversity-atom.fresh ];
then
xsltproc `dirname $0`/atom.xslt `dirname $0`/atom.xml > /tmp/VillageUniversity-atom.tmp
touch /tmp/VillageUniversity-atom.fresh
fi
