<?php
$opts = array(
    'http' => array(
        'user_agent' => 'PHP libxml agent',
    )
);

$context = stream_context_create($opts);
libxml_set_streams_context($context);

header("Generator: http://atom.geekhood.net");
$url = $_GET["url"];
$chan = new DOMDocument(); $chan->load($url); /* load channel */
$sheet = new DOMDocument(); $sheet->load('atom2rss.xsl'); /* use stylesheet from this page */
$processor = new XSLTProcessor();
$processor->registerPHPFunctions();
$processor->importStylesheet($sheet);
$result = $processor->transformToXML($chan); /* transform to XML string (there are other options - see PHP manual)  */
echo $result;
?>
